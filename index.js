let http = require("http");

var port = 3000

const server = http.createServer((request, response) => {

	if (request.url == '/login') {
		response.writeHead(200, {'Contet-Type': 'text/plain'});
		response.end("This is the login page");
	} else {
		response.writeHead(404, {'Contet-Type': 'text/plain'});
		response.end("Page not available!");
	};

});

server.listen(port);

console.log(`Server is successfully running at localhost: ${port}`);